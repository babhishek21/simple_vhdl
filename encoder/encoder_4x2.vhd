library ieee;
use ieee.std_logic_1164.all;

entity encoder4x2 is
  port( lin: in std_logic_vector(3 downto 0);
        lout: out std_logic_vector(1 downto 0) );
end entity;

architecture encoder4x2_arch of encoder4x2 is
begin

  -- method 1: selected signal assignment (with/select)
  with lin select lout <=
    "00" when "0001",
    "01" when "0010",
    "10" when "0100",
    "11" when "1000",
    "ZZ" when others;

  -- method 2: conditional signal assignment (when/else)

  -- method 3: combiantional sequential assignment (case or if-else)
  -- (sequential, hence requires a process)

end architecture;
