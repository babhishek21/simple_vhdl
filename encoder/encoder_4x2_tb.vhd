library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity encoder4x2_tb is
end entity;

architecture encoder4x2_tb_arch of encoder4x2_tb is
  component encoder4x2 is
    port( lin: in std_logic_vector(3 downto 0);
          lout: out std_logic_vector(1 downto 0) );
  end component;

  signal linp: std_logic_vector(3 downto 0) := (0 => '1', others => '0');
  signal loutp: std_logic_vector(1 downto 0);

begin
  inst0: encoder4x2 port map(lin => linp, lout => loutp);

  encoder4x2_tb_proc: process
  begin
    linp <= (0 => '1', others => '0');
    shift_left_logical_loop: for i in 0 to 3 loop
      wait for 10 ns;
      --linp <= std_logic_vector(shift_left(unsigned(linp), 1));
       linp <= linp(2 downto 0) & '0';
    end loop;
  end process;
end architecture;
