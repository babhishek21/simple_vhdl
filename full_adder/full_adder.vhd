entity full_adder is
  port( xin, yin, cin: in bit;
        sout, cout: out bit );
end entity;

architecture full_adder_arch of full_adder is
begin
  sout <= xin xor yin xor cin;
  cout <= (xin and yin) or (xin and cin) or (yin and cin);
end architecture;
