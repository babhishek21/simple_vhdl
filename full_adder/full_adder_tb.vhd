entity full_adder_tb is
end entity ;

architecture full_adder_tb_arch of full_adder_tb is
  component full_adder is
    port( xin, yin, cin: in bit;
          sout, cout: out bit );
  end component;

  signal xinp, yinp, cinp, soutp, coutp: bit;

begin
  inst0: full_adder port map(xinp, yinp, cinp, soutp, coutp);

  full_adder_tb_proc: process
  begin
    xin_loop: for i in bit loop
      yin_loop: for j in bit loop
        cin_loop: for k in bit loop
          xinp <= i;
          yinp <= j;
          cinp <= k;
          wait for 10 ns;
        end loop;
        --wait for 10 ns;
      end loop;
      --wait for 10 ns;
    end loop;
  end process;
end architecture;

