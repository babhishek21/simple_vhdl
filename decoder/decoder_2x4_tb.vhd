library ieee;
use ieee.std_logic_1164.all;

entity decoder2x4_tb is
end entity;

architecture decoder2x4_tb_arch of decoder2x4_tb is
  component decoder2x4 is
    port( lin: in std_logic_vector(1 downto 0);
          lout: out std_logic_vector(3 downto 0) );
  end component;

  signal linp: std_logic_vector(1 downto 0) := (others => '0');
  signal loutp: std_logic_vector(3 downto 0);

begin
  inst0: decoder2x4 port map(lin => linp, lout => loutp);

  decoder2x4_tb_proc: process
  begin
    bit0_loop: for i in std_logic range '0' to '1' loop
      linp(1) <= i;
      bit1_loop: for j in std_logic range '0' to '1' loop
        linp(0) <= j;
        wait for 10 ns;
      end loop;
    end loop;
  end process;
end architecture;
