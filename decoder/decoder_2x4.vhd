library ieee;
use ieee.std_logic_1164.all;

entity decoder2x4 is
  port( lin: in std_logic_vector(1 downto 0);
        lout: out std_logic_vector(3 downto 0) );
end entity;

architecture decoder2x4_arch of decoder2x4 is
begin

  -- method 1: selected signal assignment (with/select)
  with lin select lout <=
    "0001" when "00",
    "0010" when "01",
    "0100" when "10",
    "1000" when others;

  -- method 2: conditional signal assignment (when/else)
  --lout <= "0001" when lin = "00" else
  --        "0010" when lin = "01" else
  --        "0100" when lin = "10" else
  --        "1000";

  -- method 3: combiantional sequential assignment (case or if-else)
  -- (sequential, hence requires a process)
  --behav_proc: process(lin)
  --begin
    -- method 3.1: using case statement
    --case lin is
    --when "00" =>
    --  lout <= "0001";
    --when "01" =>
    --  lout <= "0010";
    --when "10" =>
    --  lout <= "0100";
    --when others =>
    --  lout <= "1000";
    --end case;

  --  -- method 3.2: using if-else statement
  --  if lin = "00" then
  --    lout <= "0001";
  --  elsif lin = "01" then
  --    lout <= "0010";
  --  elsif lin = "10" then
  --    lout <= "0100";
  --  else
  --    lout <= "1000";
  --  end if;
  --end process;


end architecture;
