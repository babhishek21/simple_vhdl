library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gray_to_binary is
  generic(N : integer := 4);
  port( gin: in std_logic_vector(N-1 downto 0);
        bout: out std_logic_vector(N-1 downto 0) );
end entity;

architecture gray_to_binary_arch of gray_to_binary is
begin

  -- This cannot be done concurrently, since
  -- output depends on a part of itself. It would
  -- be sequential hence.
  -- b3 = g3
  -- b2 = b3 xor g2
  -- b1 = b2 xor g1
  -- b0 = b1 xor g0

  process(gin)
    variable bout_temp: std_logic_vector(N-1 downto 0) := (others => '0');
  begin

    GRAYTOBIN: for i in N-1 downto 0 loop
      MSB: if i = N-1 then
        bout_temp(i) := gin(i);
      else
        bout_temp(i) := bout_temp(i+1) xor gin(i);
      end if;
    end loop;

    bout <= bout_temp;
  end process;

end architecture;
