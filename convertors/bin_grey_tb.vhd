library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bin_gray_tb is
end entity;

architecture bin_gray_tb_arch of bin_gray_tb is
  component binary_to_gray is
    generic(N : integer := 4);
    port( bin: in std_logic_vector(N-1 downto 0);
          gout: out std_logic_vector(N-1 downto 0) );
  end component;

  component gray_to_binary is
    generic(N : integer := 4);
    port( gin: in std_logic_vector(N-1 downto 0);
          bout: out std_logic_vector(N-1 downto 0) );
  end component;

  signal binp, goutp, boutp: std_logic_vector(3 downto 0);
begin
  inst0: binary_to_gray
    generic map(4)
    port map(binp, goutp);

  inst1: gray_to_binary
    generic map(4)
    port map(goutp, boutp);

  testbench_proc: process
  begin
    all_combos: for i in std_logic range '0' to '1' loop
      binp(3) <= i;
      for j in std_logic range '0' to '1' loop
        binp(2) <= j;
        for k in std_logic range '0' to '1' loop
          binp(1) <= k;
          for l in std_logic range '0' to '1' loop
            binp(0) <= l;
            wait for 10 ns;

            assert boutp = binp report "Epic Fail!";
          end loop;
        end loop;
      end loop;
    end loop;

  end process;

end architecture;
