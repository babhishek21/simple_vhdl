library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity binary_to_gray is
  generic(N : integer := 4);
  port( bin: in std_logic_vector(N-1 downto 0);
        gout: out std_logic_vector(N-1 downto 0) );
end entity;

architecture binary_to_gray_arch of binary_to_gray is
begin

  -- This can be done concurrently because
  -- output does not depend on part of itself.
  -- g3 = b3
  -- g2 = b3 xor b2
  -- g1 = b2 xor b1
  -- g0 = b1 xor b0

  BINTOGRAY: for i in N-1 downto 0 generate
    MSB: if i = N-1 generate
      gout(i) <= bin(i);
    end generate;

    OTHERB: if i < N-1 generate
      gout(i) <= bin(i+1) xor bin(i);
    end generate;
  end generate;

end architecture;
