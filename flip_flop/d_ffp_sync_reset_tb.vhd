library ieee;
use ieee.std_logic_1164.all;

entity dffp_syncr_tb is
end entity;

architecture dffp_syncr_tb_arch of dffp_syncr_tb is
  component dffp_syncr is
    port( D, clk, rst: in std_logic;
          Q: out std_logic );
  end component;

  signal Din, clkp, rstp, Qout: std_logic;

begin
  inst0: dffp_syncr port map(Din, clkp, rstp, Qout);

  -- clk pulse process
  clk_pulse: process
  begin
    clkp <= '0';
    wait for 10 ns;
    clkp <= '1';
    wait for 10 ns;
  end process;

  -- rst pulse process
  rst_pulse: process
  begin
    rstp <= '0';
    wait for 50 ns;
    rstp <= '1';
    wait for 10 ns;
  end process;

  -- testbench process
  process
  begin
    Din <= '0';
    wait for 7 ns;
    Din <= '1';
    wait for 7 ns;
  end process;
end architecture;
