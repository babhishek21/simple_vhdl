library ieee;
use ieee.std_logic_1164.all;

entity dffp_syncr is
  port( D, clk, rst: in std_logic;
        Q: out std_logic );
end entity;

architecture dffp_syncr_arch of dffp_syncr is
begin
  arch_process: process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        Q <= 'Z';
      else
        Q <= D;
      end if;
    end if;
  end process;
end architecture;
