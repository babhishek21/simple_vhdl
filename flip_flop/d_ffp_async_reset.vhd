library ieee;
use ieee.std_logic_1164.all;

entity dffp_asyncr is
  port( D, clk, rst: in std_logic;
        Q: out std_logic );
end entity;

architecture dffp_asyncr_arch of dffp_asyncr is
begin
  arch_process: process(clk, rst)
  begin
    if rst = '1' then
      Q <= 'Z';
    elsif rising_edge(clk) then
      Q <= D;
    end if;
  end process;
end architecture;
