library ieee;
use ieee.std_logic_1164.all;

entity dffp_asyncr_tb is
end entity;

architecture dffp_asyncr_tb_arch of dffp_asyncr_tb is
  component dffp_asyncr is
    port( D, clk, rst: in std_logic;
          Q: out std_logic );
  end component;

  signal Din, clkp, rstp, Qout: std_logic;

begin
  inst0: dffp_asyncr port map(Din, clkp, rstp, Qout);

  -- clk pulse process
  clk_pulse: process
  begin
    clkp <= '0';
    wait for 12 ns;
    clkp <= '1';
    wait for 12 ns;
  end process;

  -- rst pulse process
  rst_pulse: process
  begin
    rstp <= '0';
    wait for 35 ns;
    rstp <= '1';
    wait for 5 ns;
  end process;

  -- testbench process
  process
  begin
    Din <= '0';
    wait for 7 ns;
    Din <= '1';
    wait for 7 ns;
  end process;
end architecture;
